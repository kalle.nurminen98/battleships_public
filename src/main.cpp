#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <cmath>

const int ROWS = 10;
const int COLUMNS = 10;
const int CARRIER = 5;
const int BATTLESHIP = 4;
const int CRUISER = 3;
const int SUBMARINE = 3;
const int DESTROYER = 2;
int max_ships = 5; 
int ship_size; 

int matrix[ROWS][COLUMNS];
int cmatrix[ROWS][COLUMNS];

void initializing_computer_board()
{    
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            cmatrix[i][j] = 0;
        }
    }
}
void initializing_player_board()
{    
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            matrix[i][j] = 0;
        }
    }
}

void show_computer_board()
{
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            std::cout << "'" << cmatrix[i][j] << "' ";
        }
        std::cout << std::endl;
    }
}
void show_player_board()
{
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            std::cout << "'" << matrix[i][j] << "' ";
        }
        std::cout << std::endl;
    }
}
// Sets ships random for user
void set_ships_random_user()
{
    int count_of_ships = 0;
    int random_direction;
    std::string ship_name;
    bool check = true;
    // Going through every ship
    while(count_of_ships < max_ships)
    {
        // random position
        int x = rand() % ROWS;
        int y = rand() % COLUMNS;  
        if(count_of_ships == 0)
        {
            ship_size = CARRIER;
            ship_name = "Carrier";
        }
        else if(count_of_ships == 1)
        {
            ship_size = BATTLESHIP;
            ship_name = "Battleship";
        }
        else if(count_of_ships == 2)
        {
            ship_size = CRUISER;
            ship_name = "Cruiser";
        }
        else if(count_of_ships == 3)
        {
            ship_size = SUBMARINE;
            ship_name = "Submarine";
        }
        else if(count_of_ships == 4)
        {
            ship_size = DESTROYER;
            ship_name = "Destroyer";
        }
        // If in that poition theres no ship yet
        if(matrix[x][y] == 0)
        {
            bool check = true;
            // 1 = west, 2 = south, 3 = east, 4 = north
            random_direction = rand() % 4 + 1;
            // Puts value for that position
            matrix[x][y] = 1;
            // If rest of the ship is going to west
            if(random_direction == 1)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(matrix[x][y-i] == 1 || y-i+1 <= ship_size - 1)
                    {
                        // if false, then it generates new point to check for placement                        
                        check = false;
                        matrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check = true;
                    }                   
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        matrix[x][y-i] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to south
            else if(random_direction == 2)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(matrix[x+i][y] == 1 || x+i >= 10)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        matrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check = true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        matrix[x+i][y] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to east
            else if(random_direction == 3)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(matrix[x][y+i] == 1 || y+1 >= 10)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        matrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check=true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true && y < 10)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        matrix[x][y+i] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to north
            else if(random_direction == 4)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(matrix[x-i][y] == 1 || x-i+1 <= ship_size - 1)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        matrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check=true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        matrix[x-i][y] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }     
        }
    }
}
// Sets ships for computer
void set_ships_random_computer()
{
    int count_of_ships = 0;
    int random_direction;
    std::string ship_name;
    bool check = true;
    // Going through every ship
    while(count_of_ships < max_ships)
    {
        // random position
        int x = rand() % ROWS;
        int y = rand() % COLUMNS;    
        if(count_of_ships == 0)
        {
            ship_size = CARRIER;
            ship_name = "Carrier";
        }
        else if(count_of_ships == 1)
        {
            ship_size = BATTLESHIP;
            ship_name = "Battleship";
        }
        else if(count_of_ships == 2)
        {
            ship_size = CRUISER;
            ship_name = "Cruiser";
        }
        else if(count_of_ships == 3)
        {
            ship_size = SUBMARINE;
            ship_name = "Submarine";
        }
        else if(count_of_ships == 4)
        {
            ship_size = DESTROYER;
            ship_name = "Destroyer";
        }
        // If in that poition theres no ship yet
        if(cmatrix[x][y] == 0)
        {
            bool check = true;
            // 1 = west, 2 = south, 3 = east, 4 = north
            random_direction = rand() % 4 + 1;
            // Puts value for that position
            cmatrix[x][y] = 1;
            // If rest of the ship is going to west
            if(random_direction == 1)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(cmatrix[x][y-i] == 1 || y-i+1 <= ship_size - 1)
                    {
                        // if false, then it generates new point to check for placement                        
                        check = false;
                        cmatrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check = true;
                    }                   
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        cmatrix[x][y-i] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to south
            else if(random_direction == 2)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(cmatrix[x+i][y] == 1 || x+i >= 10)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        cmatrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check = true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        cmatrix[x+i][y] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to east
            else if(random_direction == 3)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(cmatrix[x][y+i] == 1 || y+1 >= 10)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        cmatrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check=true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true && y < 10)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        cmatrix[x][y+i] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }
            // If rest of the ship is going to north
            else if(random_direction == 4)
            {
                // checks if it can go there, true if can, false if cant
                for(int i = 1; i <ship_size; i++)
                {
                    if(cmatrix[x-i][y] == 1 || x-i+1 <= ship_size - 1)
                    {
                        // if false, then it generates new point to check for placement 
                        check = false;
                        cmatrix[x][y] = 0;
                        break;
                    }
                    else
                    {
                        check=true;
                    }
                }
                // If it can go there, then it puts it there
                if(check == true)
                {
                    for(int i = 1; i< ship_size; i++)
                    {
                        cmatrix[x-i][y] = 1;
                    }
                    // Next ship
                    count_of_ships++;  
                }
            }     
        }
    }
}
//Sets ships where user them wants
void set_ships_user_input()
{
    //configurations
    int count_of_ships = 0;
    int direction;
    std::string ship_name;
    int ending_point;
    // Going through every ship
    while(count_of_ships < max_ships)
    {
        // More configurations
        int x;
        int y;
        int west = 0;
        int south = 0;
        int east = 0;
        int north = 0;
        // Ships
        if(count_of_ships == 0)
        {
            ship_size = CARRIER;
            ship_name = "Carrier";
        }
        else if(count_of_ships == 1)
        {
            ship_size = BATTLESHIP;
            ship_name = "Battleship";
        }
        else if(count_of_ships == 2)
        {
            ship_size = CRUISER;
            ship_name = "Cruiser";
        }
        else if(count_of_ships == 3)
        {
            ship_size = SUBMARINE;
            ship_name = "Submarine";
        }
        else if(count_of_ships == 4)
        {
            ship_size = DESTROYER;
            ship_name = "Destroyer";
        }
        // User input for ships
        std::cout << "Your ship is " << ship_name << " ship size " << ship_size << std::endl;
        std::cout << "Please enter positive values for coordinates" << std::endl;
        std::cout << "First enter value for row 0-9 and then for column 0-9" << std::endl;
        std::cin >> x >> y;
        if (x < 0 || x > 9 || y < 0|| y > 9)
        {
            std::cout << "Please enter positive values inside the coordinates";
            std::cin >> x >> y;
        }
        // If there is no ship yet
        if(matrix[x][y] == 0)
        {
            // Marks that position
            matrix[x][y] = 1;
            // For loop checks if it can go there, if cant then marks destination to 1
            for(int i = 1; i <ship_size; i++)
            {
                // west
                if(matrix[x][y-i] == 1 || y-i+1 <= ship_size - 1)
                {                        
                    west = 1;
                }
                // south
                if(matrix[x+i][y] == 1 || x+i >= 10)
                {
                    south = 1;    
                }
                 // east
                if(matrix[x][y+i] == 1 || y+i >= 10)
                {
                    east = 1;
                }
                // north
                if(matrix[x-i][y] == 1 || x-i+1 <= ship_size - 1)
                {
                    north = 1;
                }
            }
            // If-statements for where it can go
            if(west == 1)
            {
                std::cout << "Cant place west" << std::endl;
            }
            if(south == 1)
            {
                std::cout << "Cant place south" << std::endl;  
            }
            if(east == 1)
            {
                std::cout << "cant place east" << std::endl;
            }
            if(north == 1)
            {
                std::cout << "Cant place north" << std::endl;
            }
            if(west == 0)
            {
                std::cout << "You can put rest of your ship to west " << ship_size -1 << std::endl;
            }
            if(south == 0)
            {
                std::cout << "You can put rest of your ship to south " << ship_size -1 << std::endl;
            }
            if(east == 0)
            {
                std::cout << "You can put rest of your ship to east " << ship_size -1 << std::endl;
            }
            if(north == 0)
            {
                std::cout << "You can put rest of your ship to north " << ship_size -1 << std::endl;
            }
            // For loop for putting it where it can go (you have 100times to get it correct)
            std::cout << "Please enter 1-4 for your ship direction if its possible destination" << std::endl;
            std::cout << "1 = west, 2 = south, 3 = east, 4 = north" << std::endl;
            std::cin >> direction;
            for(int i = 0; i < 100; i++)
            {
                if(west == 1 && direction == 1)
                {
                    std::cout << "You cant put it to west, try again " << std::endl;
                    std::cin >> direction;
                }
                if(south == 1 && direction == 2)
                {
                    std::cout << "You cant put it to south, try again " << std::endl;
                    std::cin >> direction;
                }
                if(east == 1 && direction == 3)
                {
                    std::cout << "You cant put it to east, try again " << std::endl;
                    std::cin >> direction;
                }
                if(north == 1 && direction == 4)
                {
                    std::cout << "You cant put it to north, try again " << std::endl;
                    std::cin >> direction;
                }
                else
                {
                    continue;
                }
            }
            //Switch statement to get the rest of the ship where you want it
            switch (direction)
            {
            // Puts rest of the ship to west
            case 1:
                for(int i = 1; i< ship_size; i++)
                {
                    matrix[x][y-i] = 1;
                }
                ending_point = y - ship_size + 1;
                std::cout << " Ending point " << x << "x "  << ending_point << "y " << std::endl;
                // New ship
                count_of_ships++;
                // Shows current board
                show_player_board();
                break;
            // Puts rest of the ship to south
            case 2:
                for(int i = 1; i< ship_size; i++)
                {
                    matrix[x+i][y] = 1;
                }
                ending_point = x + ship_size - 1;
                std::cout << " Ending point " << ending_point << "x "  << y << "y " << std::endl;
                // New ship
                count_of_ships++;
                // Shows current board
                show_player_board();
                break;
            // Puts rest of the ship to east
            case 3:
                for(int i = 1; i< ship_size; i++)
                {
                    matrix[x][y+i] = 1;
                }
                ending_point = y + ship_size - 1;
                std::cout << " Ending point " << x << "x "  << ending_point << "y " << std::endl;
                // New ship
                count_of_ships++;
                // Shows current board
                show_player_board();  
                break;
            // Puts rest of the ship to north
            case 4:
            for(int i = 1; i< ship_size; i++)
                {
                    matrix[x-i][y] = 1;
                }
                ending_point = x - ship_size + 1;
                std::cout << " Ending point " << ending_point << "x "  << y << "y " << std::endl;
                // New ship
                count_of_ships++;
                // Shows current board
                show_player_board();        
                break;
            default:
                std::cout << "Please enter 1-4 for your ship direction" << std::endl;
                break;
            }
        }
        // If there is ship already
        else
        {
            std::cout << "There is already a ship, try again" << std::endl;
        }
    }
}
// Counts number of ship values for player and when its 0 then player loses
int number_of_ships_player()
{
    int ships=0;
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            if(matrix[i][j] == 1)
            {
                ships++;
            }
        }
    }
    
    return ships;
}
// Counts number of ship values for computer and when its 0 then computer loses
int number_of_ships_computer()
{
    int ships=0;
    for(int i=0; i < ROWS; i++)
    {
        for(int j=0; j < COLUMNS; j++)
        {
            if(cmatrix[i][j] == 1)
            {
                ships++;
            }
        }
    }
    
    return ships;
}
// Player attack, if value is 1 then there is ship and turns it to 2 to mark that you have hit it
bool attack(int x, int y)
{
    if(cmatrix[x][y] == 1)
    {
        cmatrix[x][y] = 2;
        return true;
    }
    if(cmatrix[x][y] == 0)
    {
        cmatrix[x][y] = 4;
        return false; 
    }
    if(cmatrix[x][y] == 4)
    {
        std::cout << "You have already shot there, try again ";
        std::cin >> x >> y;
    }
    return false;    
}
bool computer_attack_cheat(int cpos1,int cpos2)
{
    while (matrix[cpos1][cpos2] != 1 | 0)
    {
            cpos1 = std::rand() % 10;
            cpos2 = std::rand() % 10;
    }

    if(matrix[cpos1][cpos2] == 1)
        {
            matrix[cpos1][cpos2] = 2;
            return true;
        }
    if(matrix[cpos1][cpos2] == 0)
    {
        matrix[cpos1][cpos2] = 4;
        return false; 
    }
 
    return false;
}
bool computer_attack_random(int cpos1,int cpos2)
{

    if(matrix[cpos1][cpos2] == 1)
        {
            matrix[cpos1][cpos2] = 2;
            return true;
        }
    if(matrix[cpos1][cpos2] == 0)
    {
        matrix[cpos1][cpos2] = 4;
        return false; 
    }
 
    return false;
}


int main()
{
    // Configurations
    srand(time(NULL));
    int choice;
    int choice_computer;
    int pos1,pos2;
    initializing_computer_board();
    initializing_player_board();
    // Game starts
    std::cout << "Welcome to this simple game of battleships " << std::endl;
    std::cout << "The computer has hidden 5 ships on a 10 x 10 board \n";
    std::cout << "Computer board:" << std::endl;
    show_computer_board();
    std::cout << "Player board:" << std::endl;
    show_player_board();
    // First choice for how you want to put ships on board
    std::cout << "The board shows 0's where no ship is, the computer will hide the ships and they will show as 1's and when hit 2's and where you have been already shot and not hit it shows 4 " << std::endl;
    std::cout << "Please enter 1 for manually putting your ships or 2 randomly putting your ships" << std::endl;
    std::cin >> choice;
    switch(choice)
    {
        case 1:
            set_ships_user_input();
            set_ships_random_computer();
            break;
        case 2:
            set_ships_random_user();
            set_ships_random_computer();
            break;
    }
    // Second choice for computer difficulty
    std::cout << "Please select if you wanna play against computer, which cheats or plays always random coordinate" << std::endl;
    std::cout << "1 for random or 2 for cheat or 3 for random and you can see computer board or 4 for cheat and you can see computer board" << std::endl;
    std::cin >> choice_computer;
    switch(choice_computer)
    {
        case 1:
            while(1)
            {
                // Random value for computer choice
                int cpos1 =10;
                int cpos2 =10;
                std::srand(std::time(NULL));
                cpos1 = std::rand() % 10;
                cpos2 = std::rand() % 10;
                // Your guess
                std::cout << "Please enter the location of your guess (row then column, numbers 0-9 for example 0 9 is the right upper corner): ";
                std::cin >> pos1 >> pos2;

                if (pos1 < 0 || pos1 > 9 || pos2 < 0|| pos2 > 9)
                {
                    std::cout << "Please enter positive values inside the coordinates";
                    std::cin >> pos1 >> pos2;
                }
                std::cout <<  std::endl;
                // If you hit target
                if(attack(pos1,pos2))
                {
                    std::cout << "You hit computer ship" << std::endl;
                }
                // you didn't hit target
                else
                {
                    std::cout << "Sorry there is no ship at this location, please try again" << std::endl;
                }
                // Computer attacks
                if(computer_attack_random(cpos1, cpos2))
                {
                    std::cout << "Computer hit your ship" << std::endl;
                }
                else
                {
                    std::cout << "Computer misses" << std::endl;
                }
                // Counts values after attacks
                number_of_ships_player();
                number_of_ships_computer();
                // You dont have ships anymore
                if (number_of_ships_player() == 0)
                {
                    std::cout << "Game over, you lost" << std::endl;
                    break;
                }
                // Computer dont have ships anymore
                if (number_of_ships_computer() == 0)
                {
                    std::cout << "Game over, you win" << std::endl;
                    break;
                }
                std::cout <<  std::endl;
                std::cout << "Your board" << std::endl;
                show_player_board();
                std::cout <<  std::endl;
                std::cout <<  std::endl;
            }
            break;
        case 2:
            while(1)
            {
                // Random value for computer choice
                int cpos1 =10;
                int cpos2 =10;
                std::srand(std::time(NULL));
                cpos1 = std::rand() % 10;
                cpos2 = std::rand() % 10;
                // Your guess
                std::cout << "Please enter the location of your guess (row then column, numbers 0 9 is the right upper corner): ";
                std::cin >> pos1 >> pos2;

                if (pos1 < 0 || pos1 > 9 || pos2 < 0|| pos2 > 9)
                {
                    std::cout << "Please enter positive values inside the coordinates";
                    std::cin >> pos1 >> pos2;
                }
                std::cout <<  std::endl;
                // If you hit target
                if(attack(pos1,pos2))
                {
                    std::cout << "You hit computer ship" << std::endl;
                }
                // you didn't hit target
                else
                {
                    std::cout << "Sorry there is no ship at this location, please try again" << std::endl;
                }
                // Computer attacks
                if(computer_attack_cheat(cpos1, cpos2))
                {
                    std::cout << "Computer hit your ship" << std::endl;
                }
                else
                {
                    std::cout << "Computer misses" << std::endl;
                }
                // Counts values after attacks
                number_of_ships_player();
                number_of_ships_computer();
                // You dont have ships anymore
                if (number_of_ships_player() == 0)
                {
                    std::cout << "Game over, you lost" << std::endl;
                    break;
                }
                // Computer dont have ships anymore
                if (number_of_ships_computer() == 0)
                {
                    std::cout << "Game over, you win" << std::endl;
                    break;
                }
                std::cout <<  std::endl;
                std::cout << "Your board" << std::endl;
                show_player_board();
                std::cout <<  std::endl;
                std::cout <<  std::endl;
            }
            break;
        case 3:
            while(1)
            {
                // Random value for computer choice
                int cpos1 =10;
                int cpos2 =10;
                std::srand(std::time(NULL));
                cpos1 = std::rand() % 10;
                cpos2 = std::rand() % 10;
                std::cout <<  std::endl;
                std::cout << "Computer board" << std::endl;
                show_computer_board();
                std::cout <<  std::endl;
                // Your guess
                std::cout << "Please enter the location of your guess (row then column, numbers 0-9 for example 0 9 is the right upper corner): ";
                std::cin >> pos1 >> pos2;

                if (pos1 < 0 || pos1 > 9 || pos2 < 0|| pos2 > 9)
                {
                    std::cout << "Please enter positive values inside the coordinates";
                    std::cin >> pos1 >> pos2;
                }
                std::cout <<  std::endl;
                // If you hit target
                if(attack(pos1,pos2))
                {
                    std::cout << "You hit computer ship" << std::endl;
                }
                // you didn't hit target
                else
                {
                    std::cout << "Sorry there is no ship at this location, please try again" << std::endl;
                }
                // Computer attacks
                if(computer_attack_random(cpos1, cpos2))
                {
                    std::cout << "Computer hit your ship" << std::endl;
                }
                else
                {
                    std::cout << "Computer misses" << std::endl;
                }
                // Counts values after attacks
                number_of_ships_player();
                number_of_ships_computer();
                // You dont have ships anymore
                if (number_of_ships_player() == 0)
                {
                    std::cout << "Game over, you lost" << std::endl;
                    break;
                }
                // Computer dont have ships anymore
                if (number_of_ships_computer() == 0)
                {
                    std::cout << "Game over, you win" << std::endl;
                    break;
                }
                std::cout <<  std::endl;
                std::cout << "Your board" << std::endl;
                show_player_board();
                std::cout <<  std::endl;
                std::cout <<  std::endl;

            }
            break;
        case 4:
            while(1)
            {
                // Random value for computer choice
                int cpos1 =10;
                int cpos2 =10;
                std::srand(std::time(NULL));
                cpos1 = std::rand() % 10;
                cpos2 = std::rand() % 10;
                std::cout <<  std::endl;
                std::cout << "Computer board" << std::endl;
                show_computer_board();
                std::cout <<  std::endl;

                // Your guess
                std::cout << "Please enter the location of your guess (row then column, numbers 0 9 is the right upper corner): ";
                std::cin >> pos1 >> pos2;

                if (pos1 < 0 || pos1 > 9 || pos2 < 0|| pos2 > 9)
                {
                    std::cout << "Please enter positive values inside the coordinates";
                    std::cin >> pos1 >> pos2;
                }
                std::cout <<  std::endl;
                // If you hit target
                if(attack(pos1,pos2))
                {
                    std::cout << "You hit computer ship" << std::endl;
                }
                // you didn't hit target
                else
                {
                    std::cout << "Sorry there is no ship at this location, please try again" << std::endl;
                }
                // Computer attacks
                if(computer_attack_cheat(cpos1, cpos2))
                {
                    std::cout << "Computer hit your ship" << std::endl;
                }
                else
                {
                    std::cout << "Computer misses" << std::endl;
                }
                // Counts values after attacks
                number_of_ships_player();
                number_of_ships_computer();
                // You dont have ships anymore
                if (number_of_ships_player() == 0)
                {
                    std::cout << "Game over, you lost" << std::endl;
                    break;
                }
                // Computer dont have ships anymore
                if (number_of_ships_computer() == 0)
                {
                    std::cout << "Game over, you win" << std::endl;
                    break;
                }
                std::cout <<  std::endl;
                std::cout << "Your board" << std::endl;
                show_player_board();
                std::cout <<  std::endl;
                std::cout <<  std::endl;

            }
            break;
    }
    return 0;
}