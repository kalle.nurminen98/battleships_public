## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
Battleships game

### Short Description
Classic battleship game played on terminal. Only option to play is against a computer with randomized attacks or cheat mode.
At the beginning of the game the computer asks if you want to set your own battleships or with randomized option.
After that you start shooting the computers randomized ships and computer shoots back.

Antti Lehtosalo made: Coordinates for the boards and divided computers and players boards. Ai with randomized attacks. Made this README.md file.

Kalle Nurminen made: Configurations for boards and functions to put ships to boards (random and user input) and ship values counters and user attack. Also made main().

### Install
Make sure you have gcc version 11.1.0  
g++ main.cpp -o main and cmake

### Usage
$ mkdir build && cd build  
$ cmake ../  
$ make  
$ ./build

### Maintainer(s)

Antti Lehtosalo @mediator246
Kalle Nurminen @kalle.nurminen98

### Contributing

Antti Lehtosalo @mediator246
Kalle Nurminen @kalle.nurminen98

### License

Free to use. No license